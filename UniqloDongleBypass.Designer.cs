﻿namespace UniqloDongleBypass
{
    partial class UniqloDongleBypass
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.comboBox_port = new System.Windows.Forms.ComboBox();
            this.btn_port_open = new System.Windows.Forms.Button();
            this.richText_sended = new System.Windows.Forms.RichTextBox();
            this.richText_received = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_status = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl_server_info = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.richText_serial_sended = new System.Windows.Forms.RichTextBox();
            this.richText_serial_received = new System.Windows.Forms.RichTextBox();
            this.serialPort = new System.IO.Ports.SerialPort(this.components);
            this.button2 = new System.Windows.Forms.Button();
            this.btn_clearText = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // comboBox_port
            // 
            this.comboBox_port.FormattingEnabled = true;
            this.comboBox_port.Location = new System.Drawing.Point(12, 48);
            this.comboBox_port.Name = "comboBox_port";
            this.comboBox_port.Size = new System.Drawing.Size(143, 20);
            this.comboBox_port.TabIndex = 0;
            // 
            // btn_port_open
            // 
            this.btn_port_open.Location = new System.Drawing.Point(166, 12);
            this.btn_port_open.Name = "btn_port_open";
            this.btn_port_open.Size = new System.Drawing.Size(96, 56);
            this.btn_port_open.TabIndex = 1;
            this.btn_port_open.Text = "포트오픈";
            this.btn_port_open.UseVisualStyleBackColor = true;
            this.btn_port_open.Click += new System.EventHandler(this.btn_port_open_Click);
            // 
            // richText_sended
            // 
            this.richText_sended.Location = new System.Drawing.Point(12, 480);
            this.richText_sended.Name = "richText_sended";
            this.richText_sended.Size = new System.Drawing.Size(800, 120);
            this.richText_sended.TabIndex = 2;
            this.richText_sended.Text = "";
            // 
            // richText_received
            // 
            this.richText_received.Location = new System.Drawing.Point(12, 102);
            this.richText_received.Name = "richText_received";
            this.richText_received.Size = new System.Drawing.Size(800, 120);
            this.richText_received.TabIndex = 3;
            this.richText_received.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "Port : ";
            // 
            // lbl_status
            // 
            this.lbl_status.AutoSize = true;
            this.lbl_status.Location = new System.Drawing.Point(46, 31);
            this.lbl_status.Name = "lbl_status";
            this.lbl_status.Size = new System.Drawing.Size(29, 12);
            this.lbl_status.TabIndex = 5;
            this.lbl_status.Text = "상태";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(377, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "Server IP";
            // 
            // lbl_server_info
            // 
            this.lbl_server_info.AutoSize = true;
            this.lbl_server_info.Location = new System.Drawing.Point(442, 32);
            this.lbl_server_info.Name = "lbl_server_info";
            this.lbl_server_info.Size = new System.Drawing.Size(0, 12);
            this.lbl_server_info.TabIndex = 7;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(821, 14);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(103, 43);
            this.button1.TabIndex = 8;
            this.button1.Text = "ReqReset";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // richText_serial_sended
            // 
            this.richText_serial_sended.Location = new System.Drawing.Point(12, 228);
            this.richText_serial_sended.Name = "richText_serial_sended";
            this.richText_serial_sended.Size = new System.Drawing.Size(800, 120);
            this.richText_serial_sended.TabIndex = 9;
            this.richText_serial_sended.Text = "";
            // 
            // richText_serial_received
            // 
            this.richText_serial_received.Location = new System.Drawing.Point(12, 354);
            this.richText_serial_received.Name = "richText_serial_received";
            this.richText_serial_received.Size = new System.Drawing.Size(800, 120);
            this.richText_serial_received.TabIndex = 10;
            this.richText_serial_received.Text = "";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(821, 63);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(103, 43);
            this.button2.TabIndex = 11;
            this.button2.Text = "ReqDongleEx";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btn_clearText
            // 
            this.btn_clearText.Location = new System.Drawing.Point(699, 29);
            this.btn_clearText.Name = "btn_clearText";
            this.btn_clearText.Size = new System.Drawing.Size(96, 56);
            this.btn_clearText.TabIndex = 12;
            this.btn_clearText.Text = "Clear";
            this.btn_clearText.UseVisualStyleBackColor = true;
            this.btn_clearText.Click += new System.EventHandler(this.btn_clearText_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(821, 112);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(103, 43);
            this.button3.TabIndex = 13;
            this.button3.Text = "ReqJuminPinEx";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(933, 14);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(103, 43);
            this.button4.TabIndex = 14;
            this.button4.Text = "ReqPolling";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(821, 161);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(103, 43);
            this.button5.TabIndex = 15;
            this.button5.Text = "ReqSignEx";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // UniqloDongleBypass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1045, 613);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btn_clearText);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.richText_serial_received);
            this.Controls.Add(this.richText_serial_sended);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lbl_server_info);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbl_status);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.richText_received);
            this.Controls.Add(this.richText_sended);
            this.Controls.Add(this.btn_port_open);
            this.Controls.Add(this.comboBox_port);
            this.Name = "UniqloDongleBypass";
            this.Text = "lott";
            this.Load += new System.EventHandler(this.Form_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox_port;
        private System.Windows.Forms.Button btn_port_open;
        private System.Windows.Forms.RichTextBox richText_sended;
        private System.Windows.Forms.RichTextBox richText_received;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_status;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl_server_info;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RichTextBox richText_serial_sended;
        private System.Windows.Forms.RichTextBox richText_serial_received;
        private System.IO.Ports.SerialPort serialPort;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btn_clearText;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
    }
}

