﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UniqloDongleBypass.Comm
{
    class ReqPolling
    {
        public static byte[] MakeSendText()
        {
            byte[] tmpBuf = new byte[4096];
            int idx = 0;

            // sequence No
            tmpBuf[idx++] = Common.BYTE_STX;
            tmpBuf[idx++] = 0x00;

            // Sender Index
            tmpBuf[idx++] = 0x0B;
            tmpBuf[idx++] = 0x01;

            // Receiver index
            tmpBuf[idx++] = 0x0B;
            tmpBuf[idx++] = 0x01;

            // 동글 업무코드
            tmpBuf[idx++] = 0x07;

            // 수신 length
            tmpBuf[idx++] = 0x00;
            tmpBuf[idx++] = 0x00;
            // Data

            // CRC
            int crcLength = idx - 1;
            byte[] crcBuf = new byte[crcLength];
            Array.Copy(tmpBuf, 1, crcBuf, 0, (crcLength));
            int crc = Common.CalcCRC(crcBuf);
            tmpBuf[idx++] = (byte)(crc >> 8);
            tmpBuf[idx++] = (byte)(crc);

            tmpBuf[idx++] = Common.BYTE_ETX;

            byte[] resultBuf = new byte[idx];
            Array.Copy(tmpBuf, resultBuf, idx);

            return resultBuf;
        }
    }
}
