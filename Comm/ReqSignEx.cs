﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UniqloDongleBypass.Comm
{
    class ReqSignEx
    {
        private static byte[] GetData()
        {
            byte[] tmpBuf = new byte[4096];
            int idx = 0;

            // 서명 메시지            
            byte[] signMsgBytes = Encoding.Default.GetBytes("서명을 입력하세요.                    ");
            Array.Copy(signMsgBytes, 0, tmpBuf, idx, signMsgBytes.Length);
            idx += signMsgBytes.Length;

            // 서명완료 타임아웃
            byte[] timeoutBytes = Encoding.Default.GetBytes("02");
            Array.Copy(timeoutBytes, 0, tmpBuf, idx, timeoutBytes.Length);
            idx += timeoutBytes.Length;

            // 그룹사 로고 쇼핑
            byte[] logoBytes = Encoding.Default.GetBytes("00");
            Array.Copy(logoBytes, 0, tmpBuf, idx, logoBytes.Length);
            idx += logoBytes.Length;

            return Common.NewArrayFromBytes(tmpBuf, idx);
        }

        public static byte[] MakeSendText()
        {
            byte[] tmpBuf = new byte[4096];
            int idx = 0;

            // sequence No
            tmpBuf[idx++] = Common.BYTE_STX;
            tmpBuf[idx++] = 0x00;

            // Sender Index
            tmpBuf[idx++] = 0x0B;
            tmpBuf[idx++] = 0x01;

            // Receiver index
            tmpBuf[idx++] = 0x0B;
            tmpBuf[idx++] = 0x01;

            // 동글 업무코드
            tmpBuf[idx++] = 0xF2;

            // Data
            byte[] bodyBytes = GetData();

            // length             
            tmpBuf[idx++] = (byte)(bodyBytes.Length >> 8);
            tmpBuf[idx++] = (byte)bodyBytes.Length;

            Array.Copy(bodyBytes, 0, tmpBuf, idx, bodyBytes.Length);
            idx += bodyBytes.Length;

            // CRC
            int crcLength = idx - 1;
            byte[] crcBuf = new byte[crcLength];
            Array.Copy(tmpBuf, 1, crcBuf, 0, (crcLength));
            int crc = Common.CalcCRC(crcBuf);
            tmpBuf[idx++] = (byte)(crc >> 8);
            tmpBuf[idx++] = (byte)(crc);

            tmpBuf[idx++] = Common.BYTE_ETX;

            byte[] resultBuf = new byte[idx];
            Array.Copy(tmpBuf, resultBuf, idx);

            return resultBuf;
        }
    }
}
