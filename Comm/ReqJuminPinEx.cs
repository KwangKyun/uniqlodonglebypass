﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UniqloDongleBypass.Comm
{
    class ReqJuminPinEx
    {
        private static byte[] GetData()
        {
            byte[] tmpBuf = new byte[4096];
            int idx = 0;

            // Sub Command
            tmpBuf[idx++] = 0x01;

            // 비밀번호 길이 (16자리)
            tmpBuf[idx++] = Convert.ToByte('1');
            tmpBuf[idx++] = Convert.ToByte('6');

            // 비밀번호 표시 Type 3로
            tmpBuf[idx++] = Convert.ToByte('3');

            // 핀 숫자 종류
            tmpBuf[idx++] = Convert.ToByte('1');

            // 입력 구분
            tmpBuf[idx++] = Convert.ToByte('3');

            // 핀 암호화 방법
            tmpBuf[idx++] = Convert.ToByte('0');
     
            // 암호화 종류
            tmpBuf[idx++] = Convert.ToByte('8');

            return Common.NewArrayFromBytes(tmpBuf, idx);
        }

        public static byte[] MakeSendText()
        {
            byte[] tmpBuf = new byte[4096];
            int idx = 0;

            // sequence No
            tmpBuf[idx++] = Common.BYTE_STX;
            tmpBuf[idx++] = 0x00;

            // Sender Index
            tmpBuf[idx++] = 0x0B;
            tmpBuf[idx++] = 0x01;

            // Receiver index
            tmpBuf[idx++] = 0x0B;
            tmpBuf[idx++] = 0x01;

            // 동글 업무코드
            tmpBuf[idx++] = 0x57;

            // Data
            byte[] bodyBytes = GetData();

            // length             
            tmpBuf[idx++] = (byte)(bodyBytes.Length >> 8);
            tmpBuf[idx++] = (byte)bodyBytes.Length;

            Array.Copy(bodyBytes, 0, tmpBuf, idx, bodyBytes.Length);
            idx += bodyBytes.Length;

            // CRC
            int crcLength = idx - 1;
            byte[] crcBuf = new byte[crcLength];
            Array.Copy(tmpBuf, 1, crcBuf, 0, (crcLength));
            int crc = Common.CalcCRC(crcBuf);
            tmpBuf[idx++] = (byte)(crc >> 8);
            tmpBuf[idx++] = (byte)(crc);

            tmpBuf[idx++] = Common.BYTE_ETX;

            byte[] resultBuf = new byte[idx];
            Array.Copy(tmpBuf, resultBuf, idx);

            return resultBuf;
        }
    }
}
