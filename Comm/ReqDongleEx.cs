﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UniqloDongleBypass.Comm
{
    class ReqDongleEx
    {
        private static byte[] GetData()
        {
            byte[] tmpBuf = new byte[4096];
            int idx = 0;

            // Sub Command
            tmpBuf[idx++] = 0x03;
            // 거래 구분자
            tmpBuf[idx++] = Convert.ToByte('0');
            // 거래 종류
            tmpBuf[idx++] = Convert.ToByte('0');

            // 거래 일시            
            byte[] dateNowBytes = Encoding.Default.GetBytes(DateTime.Now.ToString("yyyyMMddHHmmss"));
            Array.Copy(dateNowBytes, 0, tmpBuf, idx, dateNowBytes.Length);
            idx += dateNowBytes.Length;

            // 거래 금액
            byte[] amountBytes = Encoding.Default.GetBytes("000001000");
            Array.Copy(amountBytes, 0, tmpBuf, idx, amountBytes.Length);
            idx += amountBytes.Length;

            // 단말기 ID "0"으로 패딩
            byte[] termIDBytes = Encoding.Default.GetBytes("00000000");
            Array.Copy(termIDBytes, 0, tmpBuf, idx, termIDBytes.Length);
            idx += termIDBytes.Length;

            // 그룹사 로고 쇼핑
            byte[] logoBytes = Encoding.Default.GetBytes("00");
            Array.Copy(logoBytes, 0, tmpBuf, idx, logoBytes.Length);
            idx += logoBytes.Length;

            // RFU
            tmpBuf[idx++] = Convert.ToByte('0');

            // 암호화 종류
            tmpBuf[idx++] = Convert.ToByte('8');

            return Common.NewArrayFromBytes(tmpBuf, idx);
        }

        public static byte[] MakeSendText()
        {
            byte[] tmpBuf = new byte[4096];
            int idx = 0;

            // sequence No
            tmpBuf[idx++] = Common.BYTE_STX;
            tmpBuf[idx++] = 0x00;

            // Sender Index
            tmpBuf[idx++] = 0x0B;
            tmpBuf[idx++] = 0x01;

            // Receiver index
            tmpBuf[idx++] = 0x0B;
            tmpBuf[idx++] = 0x01;

            // 동글 업무코드
            tmpBuf[idx++] = 0x57;
            
            // Data
            byte[] bodyBytes = GetData();

            // length             
            tmpBuf[idx++] = (byte)(bodyBytes.Length >> 8 );
            tmpBuf[idx++] = (byte)bodyBytes.Length;

            Array.Copy(bodyBytes, 0, tmpBuf, idx, bodyBytes.Length);
            idx += bodyBytes.Length;

            // CRC
            int crcLength = idx - 1;
            byte[] crcBuf = new byte[crcLength];
            Array.Copy(tmpBuf, 1, crcBuf, 0, (crcLength));
            int crc = Common.CalcCRC(crcBuf);
            tmpBuf[idx++] = (byte)(crc >> 8);
            tmpBuf[idx++] = (byte)(crc);

            tmpBuf[idx++] = Common.BYTE_ETX;

            byte[] resultBuf = new byte[idx];
            Array.Copy(tmpBuf, resultBuf, idx);

            return resultBuf;
        }
    }
}
