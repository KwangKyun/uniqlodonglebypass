﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.IO.Ports;
using System.Diagnostics;
using UniqloDongleBypass.Comm;

namespace UniqloDongleBypass
{
    public partial class UniqloDongleBypass : Form
    {
        // 시리얼포트 관련 핸들러
        public delegate void DataReceivedHandlerFunc(byte[] receiveData);
        public DataReceivedHandlerFunc DataReceivedHandler;

        // 소켓 관련 핸들러
        delegate void AppendTextDelegate(Control ctrl, string s);
        AppendTextDelegate _textAppender;
        Socket mainSock;
        IPAddress thisAddress;

        byte[] tmpSerialBuffer = new byte[4096*2];
        int tmpSerialBufferSize = 0;


        // 연결된 클라이언트 리스트
        List<Socket> connectedClients = new List<Socket>();

        public UniqloDongleBypass()
        {
            InitializeComponent();
            mainSock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
            _textAppender = new AppendTextDelegate(AppendText);
        }

        void AppendText(Control ctrl, string s)
        {
            if (ctrl.InvokeRequired) ctrl.Invoke(_textAppender, ctrl, s);
            else
            {
                string source = ctrl.Text;
                if(string.IsNullOrEmpty(source))
                {
                    ctrl.Text = s;
                }
                else
                {
                    ctrl.Text = source + Environment.NewLine + s;
                }
            }
        }        

        private void Form_Load(object sender, EventArgs e)
        {
            IPHostEntry he = Dns.GetHostEntry(Dns.GetHostName());

            // 처음으로 발견되는 ipv4 주소를 사용한다.
            foreach (IPAddress addr in he.AddressList)
            {
                if (addr.AddressFamily == AddressFamily.InterNetwork)
                {
                    thisAddress = addr;
                    break;
                }
            }

            // 주소가 없다면..
            if (thisAddress == null)
                // 로컬호스트 주소를 사용한다.
                thisAddress = IPAddress.Loopback;

            lbl_server_info.Text = thisAddress.ToString();


            // 연결 가능한 Serial Port 
            comboBox_port.DataSource = SerialPort.GetPortNames();
        }

        void AcceptCallback(IAsyncResult ar)
        {
            // 클라이언트의 연결 요청을 수락한다.
            Socket client = mainSock.EndAccept(ar);

            // 또 다른 클라이언트의 연결을 대기한다.
            mainSock.BeginAccept(AcceptCallback, null);

            AsyncObject obj = new AsyncObject(4096);
            obj.WorkingSocket = client;

            // 연결된 클라이언트 리스트에 추가해준다.
            connectedClients.Add(client);

            // 텍스트박스에 클라이언트가 연결되었다고 써준다.            
            AppendText(richText_received, string.Format("클라이언트 (@ {0})가 연결되었습니다.", client.RemoteEndPoint));

            // 클라이언트의 데이터를 받는다.
            client.BeginReceive(obj.Buffer, 0, 4096, 0, DataReceived, obj);
        }

        void DataReceived(IAsyncResult ar)
        {
            // BeginReceive에서 추가적으로 넘어온 데이터를 AsyncObject 형식으로 변환한다.
            AsyncObject obj = (AsyncObject)ar.AsyncState;

            // 데이터 수신을 끝낸다.
            int received = obj.WorkingSocket.EndReceive(ar);

            // 받은 데이터가 없으면(연결끊어짐) 끝낸다.
            if (received <= 0)
            {
                obj.WorkingSocket.Close();
                return;
            }

            // 텍스트로 변환한다.            
            string text = Common.ConvertByteToHexString(obj.Buffer, received);
            
            // 텍스트박스에 추가해준다.
            // 비동기식으로 작업하기 때문에 폼의 UI 스레드에서 작업을 해줘야 한다.
            // 따라서 대리자를 통해 처리한다.
            AppendText(richText_received, string.Format("[받음] : {0}", text));

            // 데이터를 받은 후엔 다시 버퍼를 비워주고 같은 방법으로 수신을 대기한다.
            obj.ClearBuffer();

            // 시리얼 포트로 해당 데이터를 전송한다.
            AppendText(richText_serial_sended, string.Format("[시리얼전송] : {0}", text));
            serialPort_DataSend(text);
            // 수신 대기
            //obj.WorkingSocket.BeginReceive(obj.Buffer, 0, 4096, 0, DataReceived, obj);
        }

        void OnSendData(object sender, EventArgs e, string sendText)
        {
            // 서버가 대기중인지 확인한다.
            if (!mainSock.IsBound)
            {                
                MsgBoxHelper.Warn("서버가 실행되고 있지 않습니다!");
                return;
            }

            // 보낼 텍스트
            string tts = sendText; 

            // 문자열을 byte[]로 변경하여 전송한다.
            byte[] bDts = Common.ConvertHexStringToByte(tts);

            // 연결된 모든 클라이언트에게 전송한다.
            for (int i = connectedClients.Count - 1; i >= 0; i--)
            {
                Socket socket = connectedClients[i];
                try {
                    socket.Send(bDts);                    
                    AppendText(richText_sended, string.Format("[보냄]{0}: {1}", thisAddress.ToString(), tts));

                    if(bDts[0] == Common.BYTE_STX)
                    {
                        connectedClients.RemoveAt(i);
                        socket.Close();
                        TempBufferClear();
                    }                    
                }
                catch
                {
                    // 오류 발생하면 전송 취소하고 리스트에서 삭제한다.
                    try { socket.Dispose(); } catch { }
                    connectedClients.RemoveAt(i);
                    AppendText(richText_sended, string.Format("[전송이 취소됨]"));
                }
            }
        }

        private void btn_port_open_Click(object sender, EventArgs e)
        {
            lbl_status.Text = "";

            int port = 10001;

            // 서버에서 클라이언트의 연결 요청을 대기하기 위해
            // 소켓을 열어둔다.
            IPEndPoint serverEP = new IPEndPoint(thisAddress, port);
            mainSock.Bind(serverEP);
            mainSock.Listen(10);

            // 비동기적으로 클라이언트의 연결 요청을 받는다.
            mainSock.BeginAccept(AcceptCallback, null);
            
            if (!serialPort.IsOpen)  //시리얼포트가 열려 있지 않으면
            {
                serialPort.PortName = comboBox_port.Text;  //콤보박스의 선택된 COM포트명을 시리얼포트명으로 지정
                serialPort.BaudRate = 115200;  //보레이트 변경이 필요하면 숫자 변경하기
                serialPort.DataBits = 8;
                serialPort.StopBits = StopBits.One;
                serialPort.Parity = Parity.None;
                serialPort.DataReceived += new SerialDataReceivedEventHandler(serialPort_DataReceived); //이것이 꼭 필요하다

                serialPort.Open();  //시리얼포트 열기

                lbl_status.Text = "포트가 열렸습니다.";
                btn_port_open.Text = "오픈 중";
                btn_port_open.Enabled = false;
                comboBox_port.Enabled = false;
            }
            else  //시리얼포트가 열려 있으면
            {
                serialPort.Close();  //시리얼포트 닫기
                lbl_status.Text = "포트가 닫혔습니다.";
                btn_port_open.Text = "포트 오픈";
                comboBox_port.Enabled = true;  //COM포트설정 콤보박스 활성화
            }
        }

        private void serialPort_DataSend(string msg)
        {
            byte[] byteMsg = Common.ConvertHexStringToByte(msg);
            serialPort.Write(byteMsg, 0, byteMsg.Length);

            // 수신버퍼 초기화
            TempBufferClear();
        }

        private void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            this.Invoke(new EventHandler(MySerialReceived));
        }
        private byte[] ReadSerialByteDataLinux()
        {
            serialPort.ReadTimeout = 2000;
            byte[] bytesBuffer = new byte[serialPort.BytesToRead];
            int bytesToRead = serialPort.BytesToRead;
            int nReadBytes = 0;

            while ((bytesToRead - nReadBytes) > 0)
            {
                try
                {
                    bytesBuffer[nReadBytes++] = (byte)serialPort.ReadByte();
                }
                catch (TimeoutException ex)
                {
                    Debug.WriteLine(ex.ToString());
                }
            }
            return bytesBuffer;
        }

        /// <summary>
        /// 데이터 수신 메서드
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void MySerialReceived(object s, EventArgs e)  //여기에서 수신 데이타를 사용자의 용도에 따라 처리한다.
        {
            try
            {
                byte[] bytesBuffer = ReadSerialByteDataLinux();
                if (DataReceivedHandler != null)
                    DataReceivedHandler(bytesBuffer);

                Array.Copy(bytesBuffer, 0, tmpSerialBuffer, tmpSerialBufferSize, bytesBuffer.Length);
                tmpSerialBufferSize += bytesBuffer.Length;


                if (isAxisDataCheck(s, e, tmpSerialBuffer, tmpSerialBufferSize) == true)
                {
                    return;
                }

                if (isFormatCheck(tmpSerialBuffer, tmpSerialBufferSize) == true)
                {
                    // 끝에 종료문자를 수신해야 전달한다.
                    byte[] completeBuffer = Common.NewArrayFromBytes(tmpSerialBuffer, tmpSerialBufferSize);

                    string strBuffer = Common.ConvertByteToHexString(completeBuffer);
                    AppendText(richText_serial_received, string.Format("[시리얼받음] : {0}", strBuffer));
                    Debug.WriteLine("received(" + strBuffer.Length + ") : " + strBuffer);
                    OnSendData(s, e, strBuffer);

                    // 시리얼 버퍼 초기화
                    TempBufferClear();
                }                
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }

        private bool isAxisDataCheck(object s, EventArgs e, byte[] bytesBuffer, int length)
        {
            int i = 0;

            byte[] remainBytes = new byte[4096];
            int remainLength = 0;

            if (bytesBuffer[0] != 0x16)
            {
                return false;
            }

            for(i=0; i<length; i+=6)
            {
                if(i+6 > length)
                {
                    remainLength = length - i;

                    Array.Copy(bytesBuffer, i, remainBytes, 0, remainLength);
                    TempBufferClear();
                    Array.Copy(remainBytes, 0, tmpSerialBuffer, 0, remainLength);
                    tmpSerialBufferSize = remainLength;
                    return true;
                }
                byte[] axisBytes = new byte[6];
                Array.Copy(bytesBuffer, i, axisBytes, 0, 6);

                // 정상 좌표 패킷의 경우 전송
                if(axisBytes[0] == 0x16)
                {
                    OnSendData(s, e, Common.ConvertByteToHexString(axisBytes));
                }
                else
                {
                    remainLength = length - i;
                    Array.Copy(bytesBuffer, i, remainBytes, 0, remainLength);
                    TempBufferClear();
                    Array.Copy(remainBytes, 0, tmpSerialBuffer, 0, remainLength);
                    tmpSerialBufferSize = remainLength;
                    return true;
                }
            }
            

            return true;
        }

        private bool isFormatCheck(byte[] recvData, int length)
        {
            byte[] bodyLengthBuffer = new byte[4];
            int bodyLength;
            
            // 0x18 + "F" + len + data 포멧의 경우 EMV 처리중 이벤트가 발생한 문제
            if (recvData[0] == 0x18 && recvData[1] == 0x46)
            {
                bodyLengthBuffer[0] = recvData[2];
                bodyLengthBuffer[1] = 0x00;
                bodyLengthBuffer[2] = 0x00;
                bodyLengthBuffer[3] = 0x00;

                bodyLength = BitConverter.ToInt32(bodyLengthBuffer, 0);

                if( bodyLength + 3 == length)
                {
                    return true;
                }
                return false;
            }

            if (length < 9)
            {
                return false;
            }
            // little Endian 이라서 뒤집어서 저장한다.            
            bodyLengthBuffer[0] = recvData[8];
            bodyLengthBuffer[1] = recvData[7];
            bodyLengthBuffer[2] = 0x00;
            bodyLengthBuffer[3] = 0x00;
            bodyLength = BitConverter.ToInt32(bodyLengthBuffer, 0);

            // 바디 데이터 길이 + 헤더크기(12byte)
            if(bodyLength + 12 == length)
            {
                return true;
            }
            return false;
        }
        private void TempBufferClear()
        {
            Array.Clear(tmpSerialBuffer, 0, tmpSerialBufferSize);
            tmpSerialBufferSize = 0;
        }


        private void btn_clearText_Click(object sender, EventArgs e)
        {
            richText_sended.Text = "";
            richText_received.Text = "";
            richText_serial_received.Text = "";
            richText_serial_sended.Text = "";
            TempBufferClear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string sendMsg = Common.ConvertByteToHexString(ReqReset.MakeSendText());

            Thread myNewThread = new Thread(() => ClsTestClient.SendRecv(sendMsg));
            myNewThread.Start();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            string sendMsg = Common.ConvertByteToHexString(ReqDongleEx.MakeSendText());

            Thread myNewThread = new Thread(() => ClsTestClient.SendRecv(sendMsg));
            myNewThread.Start();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string sendMsg = Common.ConvertByteToHexString(ReqJuminPinEx.MakeSendText());
            Thread myNewThread = new Thread(() => ClsTestClient.SendRecv(sendMsg));
            myNewThread.Start();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string sendMsg = Common.ConvertByteToHexString(ReqPolling.MakeSendText());
            Thread myNewThread = new Thread(() => ClsTestClient.SendRecv(sendMsg));
            myNewThread.Start();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string sendMsg = Common.ConvertByteToHexString(ReqSignEx.MakeSendText());
            Thread myNewThread = new Thread(() => ClsTestClient.SendRecv(sendMsg));
            myNewThread.Start();
        }
    }
}
