﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace UniqloDongleBypass
{
    class ClsTestClient
    {
        public static bool SendRecv(string inputString)
        {
            string ipAddr = Common.GetMyIP();
            int nPortNo = 10001;

            string sendMsg = inputString;
            string recvMsg = string.Empty;

            // Data buffer for incoming data.
            byte[] bytes = new byte[4096];

            // IP 주소 변수
            IPAddress ipAddress = IPAddress.Parse(ipAddr);

            // IP 연결 지시자
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, nPortNo);

            // Create a TCP/IP  socket.
            Socket clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            clientSocket.ReceiveTimeout = 6000;

            // Connect the socket to the remote endpoint. Catch any errors.
            try
            {
                clientSocket.Connect(remoteEP);

                System.Diagnostics.Debug.WriteLine("Socket connected to {0}", clientSocket.RemoteEndPoint.ToString());
                if (clientSocket.Connected)
                {
                    byte[] data = Common.ConvertHexStringToByte(inputString);
                    int sendLen = clientSocket.Send(data);
                }

                byte[] recvBytes = new byte[8192];

                while (true)
                {
                    int ret = clientSocket.Receive(recvBytes, 8192, SocketFlags.None);
                    if (ret <= 0)
                    {
                        System.Diagnostics.Debug.WriteLine("return false");
                        return false;
                    }

                    byte[] successBytes = Common.NewArrayFromBytes(recvBytes, ret);

                    if (recvBytes[0] == 0x18)
                    {
                        recvMsg = Common.ConvertByteToHexString(successBytes);
                        System.Diagnostics.Debug.WriteLine("Socket receive Message : " + recvMsg);
                        continue;
                    }
                    else if(recvBytes[0] == 0x02)
                    {
                        recvMsg = Common.ConvertByteToHexString(successBytes);
                        System.Diagnostics.Debug.WriteLine("Socket receive Message : " + recvMsg);
                        break;
                    }
                    else
                    {
                        recvMsg = Common.ConvertByteToHexString(successBytes);
                        System.Diagnostics.Debug.WriteLine("Socket receive Message : " + recvMsg);
                        continue;
                    }
                }

                System.Diagnostics.Debug.WriteLine("Socket 종료");
            }
            catch (ArgumentNullException ane)
            {
                Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
                return false;
            }
            catch (SocketException se)
            {
                Console.WriteLine("SocketException : {0}", se.ToString());
                return false;
            }
            catch (Exception etc)
            {
                Console.WriteLine("Exception : {0}", etc.ToString());
                return false;
            }
            finally
            {
                System.Diagnostics.Debug.WriteLine("Socket Close");
                clientSocket.Close();
            }

            return true;
        }
    }
}
